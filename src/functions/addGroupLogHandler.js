'use strict';
const pool = require('./config/config');
const sqlScripts = require('./config/sqlScripts');
var EffectiveDate = '';

module.exports.addGroupLogHandler = (event) => {

    try {
        return new Promise((resolve, reject) => {
            console.log(event);
            const data = JSON.parse(event.body);
            //const data = event;
            pool.getConnection(function (err, connection) {
                if (err) reject(err);
                else {
                    var sql = '';
                    EffectiveDate = getFormattedEffectiveDate(data.EffectiveDate);

                    sql = sqlScripts.
                        insertGroupLogSQL(
                            data.GroupName, data.GroupDesc, data.GroupOrder,
                            data.ModUser, EffectiveDate, data.Enabled
                        );
                    console.log(sql);
                    executeQuery(sql, connection, resolve, reject, data);
                }
            })
        })
    } catch (err) {
        console.log(err);
    }

    function executeQuery(sql, connection, resolve, reject, data) {
        connection.query(sql, function (err, result) {
            console.log('sql' + sql);
            connection.release();
            if (err) reject(err);
            else {
                const response = {
                    statusCode: 200,
                    headers: {
                        "Access-Control-Allow-Origin": "*",
                        "Access-Control-Allow-Credentials": true
                    },
                    body: JSON.stringify({
                        "/grouplog/add": {
                            "GrouplogAdd": {
                                "GroupKey": result[1].insertId,
                                "GroupName": data.GroupName,
                                "GroupDesc": data.GroupDesc,
                                "GroupOrder": data.GroupOrder,
                                "ModDate": getFormattedModDate(),
                                "ModUser": result[3][0].USER_NAME,
                                "EffectiveDate": EffectiveDate,
                                "Enabled": data.Enabled
                            }
                        }
                    })
                };
                console.log(response);
                resolve(response);
            }
        })
    }

    function getFormattedEffectiveDate(effectiveDate) {

        if (effectiveDate && effectiveDate != ""
            && effectiveDate != null && effectiveDate.length == 8) {
            const year = effectiveDate.substring(0, 4);
            const month = effectiveDate.substring(4, 6);
            const day = effectiveDate.substring(6);
            console.log(year);
            console.log(month);
            console.log(day);
            return year + '-' + month + '-' + day;

        }
    }

    function getFormattedModDate() {
        var today = new Date();
        var dd = today.getDate();

        var mm = today.getMonth() + 1;
        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        return today = yyyy+ '-'+mm+'-' +dd;
        
    }

}