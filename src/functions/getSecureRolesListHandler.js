'use strict';
const pool = require('./config/config');
const sqlScripts = require('./config/sqlScripts');


module.exports.getSecureRolesListHandler = (event) => {

    try {
        return new Promise((resolve, reject) => {
            console.log(event);
            pool.getConnection(function (err, connection) {
                if (err) reject(err);
                else {
                    var sql = '';
                    sql = sqlScripts.selectSecureRolesList();
                    console.log(sql);
                    executeQuery(sql, connection, resolve, reject);
                }
            })
        })
    } catch (err) {
        console.log(err);
    }

    function executeQuery(sql, connection, resolve, reject) {
        connection.query(sql, function (err, result) {
            console.log('sql' + sql);
            connection.release();
            if (err) reject(err);
            else {
                const response = {
                    statusCode: 200,
                    headers: {
                        "Access-Control-Allow-Origin": "*",
                        "Access-Control-Allow-Credentials": true
                    },
                    body: JSON.stringify({
                        "/securerole/viewall": {
                            "secureRoleList": result
                        }
                    })
                };
                console.log(response);
                resolve(response);
            }
        })
    }
}