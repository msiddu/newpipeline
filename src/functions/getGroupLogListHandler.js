'use strict';
const pool = require('./config/config');
const sqlScripts = require('./config/sqlScripts');


module.exports.getGroupLogListHandler = (event) => {

    try {
        return new Promise((resolve, reject) => {
            console.log(event);
            //const data = JSON.parse(event.body);
            //const data = event;
            pool.getConnection(function (err, connection) {
                if (err) reject(err);
                else {
                    if (event) {
                        var sql = '';
                        sql = sqlScripts.selectGroupLogListSQL();
                        console.log(sql);
                        executeQuery(sql, connection, resolve, reject);
                    }
                }
            })
        })
    } catch (err) {
        console.log(err);
    }

    function executeQuery(sql, connection, resolve, reject) {
        connection.query(sql, function (err, result) {
            console.log('sql'+sql);
            connection.release();
            if (err) reject(err);
            else {
                const response = {
                    statusCode: 200,
                    headers: {
                        "Access-Control-Allow-Origin": "*",
                        "Access-Control-Allow-Credentials": true
                    },
                    body: JSON.stringify({
                        "/grouplog/view/all": {
                            "Group": result
                        }
                    })
                };
                console.log(response);
                resolve(response);
            }
        })
    }
}