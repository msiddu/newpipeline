'use strict';
const pool = require('./config/config');
const sqlScripts = require('./config/sqlScripts');


module.exports.getKPIFamilyInputViewHandler = (event) => {

    try {
        return new Promise((resolve, reject) => {
            console.log(event);
            const data = JSON.parse(event.body);
            //const data = event;
            var KPIInputKey,KPIInputFiscalYear,FamilyKey = '';
            KPIInputKey = data.KPIInputKey;
            KPIInputFiscalYear = data.KPIInputFiscalYear;
            FamilyKey = data.FamilyKey;
            pool.getConnection(function (err, connection) {
                if (err) reject(err);
                else {
                    if (!KPIInputKey || KPIInputKey == undefined
                        || KPIInputKey == null
                        || KPIInputKey == "" ||
                        !KPIInputFiscalYear || KPIInputFiscalYear == undefined
                        || KPIInputFiscalYear == null
                        || KPIInputFiscalYear == "" ||
                        !FamilyKey || FamilyKey == undefined
                        || FamilyKey == null
                        || FamilyKey == "") {
                        const response = {
                            statusCode: 400,
                            headers: {
                                "Access-Control-Allow-Origin": "*",
                                "Access-Control-Allow-Credentials": true
                            },
                            body: JSON.stringify({
                                "/FamilyInputValue/view/1": {
                                    "InputValueAll": "bad input parameter - no operators"
                                }
                            })
                        };
                        console.log(response);
                        resolve(response);
                    }
                    else {
                        var sql = '';
                        sql = sqlScripts.selectKPIFamilyInputValueViewSQL(KPIInputKey,KPIInputFiscalYear,FamilyKey);
                        console.log(sql);
                        executeQuery(sql, connection, resolve, reject);
                    }
                }
            })
        })
    } catch (err) {
        console.log(err);
    }

    function executeQuery(sql, connection, resolve, reject) {
        connection.query(sql, function (err, result) {
            console.log('sql' + sql);
            connection.release();
            if (err) reject(err);
            else {
                const response = {
                    statusCode: 200,
                    headers: {
                        "Access-Control-Allow-Origin": "*",
                        "Access-Control-Allow-Credentials": true
                    },
                    body: JSON.stringify({
                        "/FamilyInputValue/view/1": {
                            "InputValueAll" : result
                        }
                    })
                };
                console.log(response);
                resolve(response);
            }
        })
    }
}