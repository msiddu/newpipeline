'use strict';
const pool = require('./config/config');
const sqlScripts = require('./config/sqlScripts');

module.exports.addKPIInputLockHandler = (event) => {

    console.log(event);
    //const data = event;
    const data = JSON.parse(event.body);
    var KPIInputKey, KPIInputTypeKey, KPIInputLocked, ModInputDate, TysonId, CurrentInd = '';
    KPIInputKey = data.KPIInputKey;
    KPIInputTypeKey = data.KPIInputTypeKey;
    KPIInputLocked = data.KPIInputLocked;
    ModInputDate = data.ModInputDate;
    TysonId = data.TysonId;
    CurrentInd = data.CurrentInd;

    try {
        return new Promise((resolve, reject) => {
            const data = event;
            pool.getConnection(function (err, connection) {
                if (err) reject(err);
                else {
                    var sql = '';
                    sql = sqlScripts.
                        insertKPIInputLockSQL(
                            KPIInputKey, KPIInputTypeKey, KPIInputLocked,
                            ModInputDate, TysonId, CurrentInd
                        );
                    executeQuery(sql, connection, resolve, reject, data);
                }
            })
        })
    } catch (err) {
        console.log(err);
    }

    function executeQuery(sql, connection, resolve, reject) {
        connection.query(sql, function (err, result) {
            console.log('sql' + sql);
            connection.release();
            if (err) {
                if (err.code == 'ER_DUP_ENTRY') {
                    const response = {
                        statusCode: 409,
                        headers: {
                            "Access-Control-Allow-Origin": "*",
                            "Access-Control-Allow-Credentials": true
                        },
                        body: JSON.stringify({
                            "/kpiInputLocked/add": {
                                "InputLockedAdd": "An existing KPI bucket already exits"
                            }
                        })
                    };
                    console.log(response);
                    resolve(response);
                }
                else {
                    reject(err)
                    console.log(err);
                }
            }
            else if (result[2].affectedRows > 0) {
                const response = {
                    statusCode: 201,
                    headers: {
                        "Access-Control-Allow-Origin": "*",
                        "Access-Control-Allow-Credentials": true
                    },
                    body: JSON.stringify({
                        "/kpiInputLocked/add": {
                            "InputLockedAdd": {
                                "KPIInputKey": KPIInputKey,
                                "KPIInputTypeKey": KPIInputTypeKey,
                                "KPIInputLocked": KPIInputLocked,
                                "CurrentInd": CurrentInd,
                                "TysonId": TysonId,
                                "ModDate": new Date()
                            }
                        }
                    })
                };
                console.log(response);
                resolve(response);
            }
        })
    }
}