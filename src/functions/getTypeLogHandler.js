'use strict';
const pool = require('./config/config');
const sqlScripts = require('./config/sqlScripts');


module.exports.getTypeLogHandler = (event) => {

    try {
        return new Promise((resolve, reject) => {
            console.log(event);
            const data = JSON.parse(event.body);
            //const data = event;
            pool.getConnection(function (err, connection) {
                if (err) reject(err);
                else {
                    if (!data.TypeKey || data.TypeKey == undefined
                        || data.TypeKey == "" || data.TypeKey == null) {
                        const response = {
                            statusCode: 400,
                            headers: {
                                "Access-Control-Allow-Origin": "*",
                                "Access-Control-Allow-Credentials": true
                            },
                            body: JSON.stringify({
                                "typelog/view/1": {
                                    "TypeOne": "bad input parameter - invalid types"
                                }
                            })
                        };
                        console.log(response);
                        resolve(response);

                    }
                    else {
                        var sql = '';
                        sql = sqlScripts.selectTypeLogSQL(data.TypeKey);
                        console.log(sql);
                        executeQuery(sql, connection, resolve, reject);
                    }
                }
            })
        })
    } catch (err) {
        console.log(err);
    }

    function executeQuery(sql, connection, resolve, reject) {
        connection.query(sql, function (err, result) {
            console.log('sql' + sql);
            connection.release();
            if (err) reject(err);
            else {
                const response = {
                    statusCode: 200,
                    headers: {
                        "Access-Control-Allow-Origin": "*",
                        "Access-Control-Allow-Credentials": true
                    },
                    body: JSON.stringify({
                        "typelog/view/1": {
                            "TypeOne": result
                        }
                    })
                };
                console.log(response);
                resolve(response);
            }
        })
    }
}