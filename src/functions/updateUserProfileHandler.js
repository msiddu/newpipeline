'use strict';
const pool = require('./config/config');

module.exports.updateUserProfileHandler = (event) => {
    try {
        return new Promise((resolve, reject) => {
            const data = JSON.parse(event.body);
            //const data = event;
            console.log(data);
            pool.getConnection(function (err, connection) {
                if (err) reject(err);
                else {
                    var sql = '';
                    const count = Object.keys(data).length;
                    console.log('How many::' + count);
                    if(count > 1 && data.TysonId){
                    sql += updateSql(data);
                    console.log(sql);
                    executeQuery(sql, connection, resolve, reject, data);
                    }else if(data.TysonId == undefined){
                        console.log('Tyson Id is null');
                        const response = {
                            statusCode: 409,
                            headers: {
                                "Access-Control-Allow-Origin": "*",
                                "Access-Control-Allow-Credentials": true
                            },
                            body: JSON.stringify({
                                "/security/local/update": {
                                    "updateLocalUser": "bad input parameter - the Tyson Id is not valid"
                                    
                                }
                            })
                        };
                        console.log(response);
                        resolve(response);
                    }
                }
            })

        })
    } catch (err) {
        console.log(err);

    }

    function executeQuery (sql, connection, resolve, reject, data){
        connection.query(sql, function (err, result) {
            connection.release();
            if (err) reject(err);
            else if(result.affectedRows == 1){

                const response = {
                    statusCode: 201,
                    headers: {
                        "Access-Control-Allow-Origin": "*",
                        "Access-Control-Allow-Credentials": true
                    },
                    body: JSON.stringify({
                        "/security/local/update": {
                            "updateLocalUser": {
                                "TysonId": data.TysonId,
                                "UserNickName ": data.UserNickName,
                                "UserPhone": data.UserPhone,
                                "UserContact": data.UserContact,
                                "UserEnabled": data.UserEnabled,
                                "UserNotes": data.UserNotes,
                                "ModUser": data.ModUser,
                                "ModDate": new Date()
                            }   
                        }
                    })
                };
                console.log(response);
                resolve(response);
            }else if(result.affectedRows == 0){

                const response = {
                    statusCode: 500,
                    headers: {
                        "Access-Control-Allow-Origin": "*",
                        "Access-Control-Allow-Credentials": true
                    },
                    body: JSON.stringify({
                        "/security/local/update": {
                            "updateLocalUser": "User does not exist with the given Tyson Id"  
                        }
                    })
                };
                console.log(response);
                resolve(response);

            }
        })
    }

     function updateSql(data){
     console.log('updateSql::'+data);
        var sql = 'update security_local set MOD_DATE = sysdate(),'
        if (data.UserNickName) sql += ` USER_NICKNAME = '${data.UserNickName}',`;
        if (data.UserPhone) sql += ` USER_PHONE = '${data.UserPhone}',`;
        if (data.UserContact) sql += ` USER_APP_CONTACT = '${data.UserContact}',`;
        if (data.UserEnabled) sql += ` ENABLED_FLAG = '${data.UserEnabled}',`;
        if (data.UserNotes) sql += ` USER_NOTES = '${data.UserNotes}',`;
        if (data.ModUser) sql += ` MOD_USER = '${data.ModUser}',`;
        

        sql = sql.substring(0, sql.length - 1) + ` where TYSON_ID = '${data.TysonId}';`

        return sql;
    }
}