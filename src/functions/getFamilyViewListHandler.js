'use strict';
const pool = require('./config/config');
const sqlScripts = require('./config/sqlScripts');


module.exports.getFamilyViewListHandler = (event) => {

    try {
        return new Promise((resolve, reject) => {
            console.log(event);
            const data = JSON.parse(event.body);
            //const data = event;
            var GroupKey = data.GroupKey;
            pool.getConnection(function (err, connection) {
                if (err) reject(err);
                else {
                    if(!GroupKey || GroupKey == undefined 
                        || GroupKey == null
                        || GroupKey == ""){
                            const response = {
                                statusCode: 400,
                                headers: {
                                    "Access-Control-Allow-Origin": "*",
                                    "Access-Control-Allow-Credentials": true
                                },
                                body: JSON.stringify({
                                    "/family/view/all": {
                                        "FamilyAll": "bad input parameter - the Tyson Id is not valid or has no groups"
                                    }
                                })
                            };
                            console.log(response);
                            resolve(response);
                        }
                   else {
                        var sql = '';
                        sql = sqlScripts.selectFamilyViewList(data.GroupKey);
                        console.log(sql);
                        executeQuery(sql, connection, resolve, reject);
                    }
                }
            })
        })
    } catch (err) {
        console.log(err);
    }

    function executeQuery(sql, connection, resolve, reject) {
        connection.query(sql, function (err, result) {
            console.log('sql'+sql);
            connection.release();
            if (err) reject(err);
            else {
                const response = {
                    statusCode: 200,
                    headers: {
                        "Access-Control-Allow-Origin": "*",
                        "Access-Control-Allow-Credentials": true
                    },
                    body: JSON.stringify({
                        "/family/view/all": {
                            "FamilyAll": result
                        }
                    })
                };
                console.log(response);
                resolve(response);
            }
        })
    }

}