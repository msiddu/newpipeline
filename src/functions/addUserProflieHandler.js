'use strict';
const pool = require('./config/config');
const sqlScripts = require('./config/sqlScripts');

module.exports.addUserProflieHandler = (event) => {
    try {
        return new Promise((resolve, reject) => {
            console.log(event);
            const data = JSON.parse(event.body);
            //const data = event;
            console.log(data);
            pool.getConnection(function (err, connection) {
                if (err) reject(err);
                else {
                    var sql = '';

                    sql = sqlScripts.insertUserProfileSQL(data.TysonId,
                        data.UserNickName, data.UserPhone, data.UserContact,
                        data.UserEnabled, data.UserNotes, data.ModUser);
                    console.log(sql);
                    executeQuery(sql, connection, resolve, reject, data);

                }
            })

        })
    } catch (err) {
        console.log(err);

    }

    function executeQuery(sql, connection, resolve, reject, data){
        connection.query(sql, function (err) {
            connection.release();
            if (err) {
                if(err.code == 'ER_DUP_ENTRY'){
                    const response = {
                        statusCode: 409,
                        headers: {
                            "Access-Control-Allow-Origin": "*",
                            "Access-Control-Allow-Credentials": true
                        },
                        body: JSON.stringify( {
                            "/security/local/add": {
                                "addLocalUser": "an existing User already exists"
                            }
                        })
                    };
                    console.log(response);
                    resolve(response);
                }else if(err.code == 'ER_NO_REFERENCED_ROW_2'){
                    const response = {
                        statusCode: 500,
                        headers: {
                            "Access-Control-Allow-Origin": "*",
                            "Access-Control-Allow-Credentials": true
                        },
                        body: JSON.stringify( {
                            "/security/local/add": {
                                "addLocalUser": "No User Exists with given TysonId in OKTA Security"
                            }
                        })
                    };
                    console.log(response);
                    resolve(response);
                }
                else{
                    console.log(err);
                    reject(err);
                }
            }
            else {
                const response = {
                    statusCode: 201,
                    headers: {
                        "Access-Control-Allow-Origin": "*",
                        "Access-Control-Allow-Credentials": true
                    },
                    body: JSON.stringify({
                        "/security/local/add": {
                            "addLocalUser":
                            {
                                "TysonId": data.TysonId,
                                "UserNickName ": data.UserNickName,
                                "UserPhone": data.UserPhone,
                                "UserContact": data.UserContact,
                                "UserEnabled": data.UserEnabled,
                                "UserNotes": data.UserNotes,
                                "ModUser": data.ModUser,
                                "ModDate": new Date()
                            }
                        }
                    })
                };
                console.log(response);
                resolve(response);
            }
        })
    }
}