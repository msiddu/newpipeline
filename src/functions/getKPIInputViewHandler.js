'use strict';
const pool = require('./config/config');
const sqlScripts = require('./config/sqlScripts');


module.exports.getKPIInputViewHandler = (event) => {

    try {
        return new Promise((resolve, reject) => {
            console.log(event);
            const data = JSON.parse(event.body);
            //const data = event;
            var KPIInputKey = '';
            var KPIInputTypeKey = '';
            var CurrentInd = '';
            KPIInputKey = data.KPIInputKey;
            KPIInputTypeKey = data.KPIInputTypeKey;
            CurrentInd = data.CurrentInd;
            pool.getConnection(function (err, connection) {
                if (err) reject(err);
                else {
                    if (!KPIInputKey || KPIInputKey == undefined
                        || KPIInputKey == null
                        || KPIInputKey == "" ||
                        !KPIInputTypeKey || KPIInputTypeKey == undefined
                        || KPIInputTypeKey == null
                        || KPIInputTypeKey == "" ||
                        !CurrentInd || CurrentInd == undefined
                        || CurrentInd == null
                        || CurrentInd == "") {
                        const response = {
                            statusCode: 400,
                            headers: {
                                "Access-Control-Allow-Origin": "*",
                                "Access-Control-Allow-Credentials": true
                            },
                            body: JSON.stringify({
                                "/kpiInputValue/view": {
                                    "InputValue": "bad input parameter - no operators"
                                }
                            })
                        };
                        console.log(response);
                        resolve(response);
                    }
                    else {
                        var sql = '';
                        sql = sqlScripts.selectKPIInputValue(KPIInputKey, KPIInputTypeKey, CurrentInd);
                        console.log(sql);
                        executeQuery(sql, connection, resolve, reject);
                    }
                }
            })
        })
    } catch (err) {
        console.log(err);
    }

    function executeQuery(sql, connection, resolve, reject) {
        connection.query(sql, function (err, result) {
            console.log('sql' + sql);
            connection.release();
            if (err) reject(err);
            else {
                const response = {
                    statusCode: 200,
                    headers: {
                        "Access-Control-Allow-Origin": "*",
                        "Access-Control-Allow-Credentials": true
                    },
                    body: JSON.stringify({
                        "/kpiInputValue/view": {
                            "InputValue": result
                        }
                    })
                };
                console.log(response);
                resolve(response);
            }
        })
    }
    
}