'use strict';
const pool = require('./config/config');
const sqlScripts = require('./config/sqlScripts');


module.exports.getKPIGroupInputViewHandler = (event) => {

    try {
        return new Promise((resolve, reject) => {
            console.log(event);
            const data = JSON.parse(event.body);
            //const data = event;
            var KPIInputKey,KPIInputFiscalYear,GroupKey = '';
            KPIInputKey = data.KPIInputKey;
            KPIInputFiscalYear = data.KPIInputFiscalYear;
            GroupKey = data.GroupKey;
            pool.getConnection(function (err, connection) {
                if (err) reject(err);
                else {
                    if (!KPIInputKey || KPIInputKey == undefined
                        || KPIInputKey == null
                        || KPIInputKey == "" ||
                        !KPIInputFiscalYear || KPIInputFiscalYear == undefined
                        || KPIInputFiscalYear == null
                        || KPIInputFiscalYear == "" ||
                        !GroupKey || GroupKey == undefined
                        || GroupKey == null
                        || GroupKey == "") {
                        const response = {
                            statusCode: 400,
                            headers: {
                                "Access-Control-Allow-Origin": "*",
                                "Access-Control-Allow-Credentials": true
                            },
                            body: JSON.stringify({
                                "/GroupInputValue/view/1": {
                                    "InputValueAll": "bad input parameter - no operators"
                                }
                            })
                        };
                        console.log(response);
                        resolve(response);
                    }
                    else {
                        var sql = '';
                        sql = sqlScripts.
                        selectKPIGroupInputValueViewSQL(KPIInputKey,KPIInputFiscalYear,GroupKey);
                        console.log(sql);
                        executeQuery(sql, connection, resolve, reject);
                    }
                }
            })
        })
    } catch (err) {
        console.log(err);
    }

    function executeQuery(sql, connection, resolve, reject) {
        connection.query(sql, function (err, result) {
            console.log('sql' + sql);
            connection.release();
            if (err) reject(err);
            else {
                const response = {
                    statusCode: 200,
                    headers: {
                        "Access-Control-Allow-Origin": "*",
                        "Access-Control-Allow-Credentials": true
                    },
                    body: JSON.stringify({
                        "/GroupInputValue/view/1": {
                            "InputValueAll" : result
                        }
                    })
                };
                console.log(response);
                resolve(response);
            }
        })
    }
}