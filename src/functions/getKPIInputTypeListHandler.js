'use strict';
const pool = require('./config/config');
const sqlScripts = require('./config/sqlScripts');


module.exports.getKPIInputTypeListHandler = (event) => {

    try {
        return new Promise((resolve, reject) => {
            console.log(event);
            const data = JSON.parse(event.body);
            //const data = event;
            var KPI_Fiscal_Year = '';
            KPI_Fiscal_Year = data.KPI_Fiscal_Year;
            if (KPI_Fiscal_Year != undefined && KPI_Fiscal_Year != '' && KPI_Fiscal_Year != null
            ) {
                pool.getConnection(function (err, connection) {
                    if (err) reject(err);
                    else {
                        var sql = '';
                        sql = sqlScripts.selectKPIInputTypeListSQL(KPI_Fiscal_Year);
                        console.log(sql);
                        executeQuery(sql, connection, resolve, reject);
                    }
                })
            } else {
                const response = {
                    statusCode: 400,
                    headers: {
                        "Access-Control-Allow-Origin": "*",
                        "Access-Control-Allow-Credentials": true
                    },
                    body: JSON.stringify({
                        "/kpiInputType/lview/all": {
                            "InputTypeAll": "bad input parameter - no operators"
                        }
                    })
                };
                console.log(response);
                resolve(response);
            }
        })
    } catch (err) {
        console.log(err);
    }

    function executeQuery(sql, connection, resolve, reject) {
        connection.query(sql, function (err, result) {
            console.log('sql' + sql);
            connection.release();
            if (err) reject(err);
            else {
                const response = {
                    statusCode: 200,
                    headers: {
                        "Access-Control-Allow-Origin": "*",
                        "Access-Control-Allow-Credentials": true
                    },
                    body: JSON.stringify({
                        "/kpiInputType/lview/all": {
                            "InputTypeAll": result
                        }
                    })
                };
                console.log(response);
                resolve(response);
            }
        })
    }
}