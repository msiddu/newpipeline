'use strict';
const pool = require('./config/config');
const sqlScripts = require('./config/sqlScripts');


module.exports.getLocalUserProfileListHandler = (event) => {

    try {
        return new Promise((resolve, reject) => {
            console.log(event);
            //const data = JSON.parse(event.body);
            //const data = event;
            pool.getConnection(function (err, connection) {
                if (err) reject(err);
                else {
                    if (event) {
                        var sql = '';
                        sql = sqlScripts.selectLocalUserProfileListSQL();
                        console.log(sql);
                        executeQuery(sql, connection, resolve, reject);
                    }
                }
            })
        })
    } catch (err) {
        console.log(err);
    }

    function executeQuery(sql, connection, resolve, reject) {
        connection.query(sql, function (err, result) {
            connection.release();
            if (err) reject(err);
            else {
                const response = {
                    statusCode: 200,
                    headers: {
                        "Access-Control-Allow-Origin": "*",
                        "Access-Control-Allow-Credentials": true
                    },
                    body: JSON.stringify({
                        "/security/corplocal/list": {
                            "listDetails": result
                        }
                    })
                };
                console.log(response);
                resolve(response);
            }
        })
    }

}