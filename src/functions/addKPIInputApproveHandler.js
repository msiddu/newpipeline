'use strict';
const pool = require('./config/config');
const sqlScripts = require('./config/sqlScripts');

module.exports.addKPIInputApproveHandler = (event) => {

    console.log(event);
    //const data = event;
    const data = JSON.parse(event.body);
    var KPIInputKey, KPIInputTypeKey, KPIInputApproved, ModInputDate, TysonId, CurrentInd = '';
    KPIInputKey = data.KPIInputKey;
    KPIInputTypeKey = data.KPIInputTypeKey;
    KPIInputApproved = data.KPIInputApproved;
    ModInputDate = data.ModInputDate;
    TysonId = data.TysonId;
    CurrentInd = data.CurrentInd;

    try {
        return new Promise((resolve, reject) => {
            const data = event;
            pool.getConnection(function (err, connection) {
                if (err) reject(err);
                else {
                    var sql = '';
                    sql = sqlScripts.
                        insertKPIInputApproveSQL(
                            KPIInputKey, KPIInputTypeKey, KPIInputApproved,
                            ModInputDate, TysonId, CurrentInd
                        );
                    executeQuery(sql, connection, resolve, reject, data);
                }
            })
        })
    } catch (err) {
        console.log(err);
    }

    function executeQuery(sql, connection, resolve, reject) {
        connection.query(sql, function (err, result) {
            console.log('sql' + sql);
            connection.release();
            if (err) {
                if (err.code == 'ER_DUP_ENTRY') {
                    const response = {
                        statusCode: 409,
                        headers: {
                            "Access-Control-Allow-Origin": "*",
                            "Access-Control-Allow-Credentials": true
                        },
                        body: JSON.stringify({
                            "/kpiInputApprove/add": {
                                "InputApproveAdd": "An existing KPI bucket already exits"
                            }
                        })
                    };
                    console.log(response);
                    resolve(response);
                }
                else {
                    reject(err)
                    console.log(err);
                }
            }
            else if (result[2].affectedRows > 0) {
                const response = {
                    statusCode: 201,
                    headers: {
                        "Access-Control-Allow-Origin": "*",
                        "Access-Control-Allow-Credentials": true
                    },
                    body: JSON.stringify({
                        "/kpiInputApprove/add": {
                            "InputApproveAdd": {
                                "KPIInputKey": KPIInputKey,
                                "KPIInputTypeKey": KPIInputTypeKey,
                                "KPIInputApproved": KPIInputApproved,
                                "CurrentInd": CurrentInd,
                                "TysonId": TysonId,
                                "ModDate": new Date()
                            }
                        }
                    })
                };
                console.log(response);
                resolve(response);
            }
        })
    }
}