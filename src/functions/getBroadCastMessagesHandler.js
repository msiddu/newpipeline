'use strict';
const pool = require('./config/config');
const sqlScripts = require('./config/sqlScripts');


module.exports.getBroadCastMessagesHandler = (event) => {

    try {
        return new Promise((resolve, reject) => {
            console.log(event);
            //const data = JSON.parse(event.body);
            const data = event;
            pool.getConnection(function (err, connection) {
                if (err) reject(err);
                else {
                   if(!data.TysonId || data.TysonId == undefined ||
                    data.TysonId == null || data.TysonId == ""){
                        const response = {
                            statusCode: 400,
                            headers: {
                                "Access-Control-Allow-Origin": "*",
                                "Access-Control-Allow-Credentials": true
                            },
                            body: JSON.stringify({
                                "/message/view/all": {
                                    "AllMessages": "bad input parameter - the Tyson Id is not valid or has no messages"
                                }
                            })
                        };
                        console.log(response);
                        resolve(response);
                   }else {
                        var sql = '';
                        sql = sqlScripts.selectAllMessages(data.TysonId);
                        console.log(sql);
                        executeQuery(sql, connection, resolve, reject);
                    }
                }
            })
        })
    } catch (err) {
        console.log(err);
    }

    function executeQuery(sql, connection, resolve, reject) {
        connection.query(sql, function (err, result) {
            connection.release();
            if (err) reject(err);
            else {
                const response = {
                    statusCode: 200,
                    headers: {
                        "Access-Control-Allow-Origin": "*",
                        "Access-Control-Allow-Credentials": true
                    },
                    body: JSON.stringify({
                        "/message/view/all": {
                            "AllMessages": result
                        }
                    })
                };
                console.log(response);
                resolve(response);
            }
        })
    }
}