'use strict';
const pool = require('./config/config');
const sqlScripts = require('./config/sqlScripts');


module.exports.editSecureUserRoleHandler = (event) => {
    console.log(event);
    //const data = JSON.parse(event.body);
    const data = event;
    var TysonID, UserRoleID, UserVLEKPI, ModUser, UserRole, UserRoleDesc = '';
    TysonID = data.TysonID;
    UserRoleID = data.UserRoleID;
    UserVLEKPI = data.UserVLEKPI;
    ModUser = data.ModUser;
    UserRole = data.UserRole;
    UserRoleDesc = data.UserRoleDesc;
    try {
        return new Promise((resolve, reject) => {
            pool.getConnection(function (err, connection) {
                if (err) reject(err);
                else {
                    if (TysonID === undefined || TysonID == ''
                        || TysonID == null) {
                        const response = {
                            statusCode: 400,
                            headers: {
                                "Access-Control-Allow-Origin": "*",
                                "Access-Control-Allow-Credentials": true
                            },
                            body: JSON.stringify({
                                "/securerole/edit": {
                                    "SecureUserRoleEdit": "invalid input, TysonId"
                                }
                            })
                        };
                        console.log(response);
                        resolve(response);
                    } else {
                        var sql = '';
                        sql = sqlScripts.
                            editSecureUserRoles(TysonID, UserRoleID, UserVLEKPI, ModUser);
                        console.log(sql);
                        executeQuery(sql, connection, resolve, reject);
                    }
                }
            })
        })
    } catch (err) {
        console.log(err);
    }

    function executeQuery(sql, connection, resolve, reject) {
        connection.query(sql, function (err, result) {
            connection.release();
            if (err) reject(err);
            else if (result.affectedRows > 0) {
                const response = {
                    statusCode: 200,
                    headers: {
                        "Access-Control-Allow-Origin": "*",
                        "Access-Control-Allow-Credentials": true
                    },
                    body: JSON.stringify({
                        "/securerole/edit": {
                            "SecureUserRoleEdit": {
                                "TysonID": TysonID,
                                "UserRoleID": UserRoleID,
                                "UserRole": UserRole,
                                "UserRoleDesc": UserRoleDesc,
                                "UserVLEKPI": UserVLEKPI,
                                "ModUser": ModUser
                            }
                        }
                    })
                };
                console.log(response);
                resolve(response);
            }else{
                const response = {
                    statusCode: 400,
                    headers: {
                        "Access-Control-Allow-Origin": "*",
                        "Access-Control-Allow-Credentials": true
                    },
                    body: JSON.stringify({
                        "/securerole/edit": {
                            "SecureUserRoleEdit": "User does not exists with the given TysonId"
                        }
                    })
                };
                console.log(response);
                resolve(response);
            }
        })
    }
}