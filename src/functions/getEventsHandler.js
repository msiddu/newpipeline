﻿var dbConnection = require('./config/config');
var sqlScripts = require('./config/sqlScripts');


    module.exports.getEventsHandler=(event)=>{
        const data = JSON.parse(event.body);
    console.log(" ** Event Name ** " +data.Name);
    console.log(" ** Event EventStartMonth ** " +data.EventStartMonth);
    console.log(" ** Event EventStartYear ** " +data.EventStartYear);
    console.log(" ** Event EventEndMonth ** " +data.EventEndMonth);
    console.log(" ** Event EventEndYear ** " +data.EventEndYear);
    console.log(" ** Event EventEnabled ** " +data.EventEnabled);

    var startYear = data.EventStartYear;
    var startMonth = data.EventStartMonth;
    var endYear = data.EventEndYear;
    var endMonth = data.EventEndMonth;
    var eventEnabled = data.EventEnabled;


    var startDate = startYear+"-"+startMonth+"-"+"01";
    var endDate = endYear+"-"+endMonth+"-"+"31";
  

   try{
        return new Promise((resolve,reject) => {
            dbConnection.getConnection(function(err,connection){

                      if(err){console.log(err); reject(err);}
                      else{
                          var sql = '';

                          if(eventEnabled){
                               
                            sql = sqlScripts.allEvents(startDate,endDate);
                            console.log(sql);
                            executeQuery(sql,connection,resolve,reject); 
                        
                          }

                      }

           });
          
        });
    }catch(err){
        console.log(err);
    }
    
}


    const executeQuery = (sql,connection,resolve,reject) => {

        connection.query(sql,function(err,result){
            connection.releaseConnection
            if(err) reject(err);
            else{
                console.log(result);
                var tmpaSecurityTag = {"{“/event/view" : {  "EventsAll" : result } }
                var response = {
                    statusCode : 200,
                    headers : {
                        "Access-Control-Allow-Origin" : "*",
                        "Access-Control-Allow-Credentials" : true
                    },
                    body : JSON.stringify(tmpaSecurityTag)
                };
                console.log(response);
               return resolve(response);
            }

        });

    }
