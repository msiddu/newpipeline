'use strict';
const pool = require('./config/config');
const sqlScripts = require('./config/sqlScripts');


module.exports.getKPIInputTypeHandler = (event) => {

    try {
        return new Promise((resolve, reject) => {
            console.log(event);
            const data = JSON.parse(event.body);
            //const data = event;
            var KPI_Fiscal_Year = '';
            var KPIInputTypeKey = '';
            KPI_Fiscal_Year = data.KPI_Fiscal_Year;
            KPIInputTypeKey = data.KPIInputTypeKey;
            if (KPI_Fiscal_Year != undefined && KPI_Fiscal_Year != '' && KPI_Fiscal_Year != null
                && KPIInputTypeKey != undefined && KPIInputTypeKey != '' && KPIInputTypeKey != null) {
                pool.getConnection(function (err, connection) {
                    if (err) reject(err);
                    else {
                        var sql = '';
                        sql = sqlScripts.selectKPIInputTypeSQL(KPI_Fiscal_Year, KPIInputTypeKey);
                        console.log(sql);
                        executeQuery(sql, connection, resolve, reject);
                    }
                })
            } else {
                const response = {
                    statusCode: 400,
                    headers: {
                        "Access-Control-Allow-Origin": "*",
                        "Access-Control-Allow-Credentials": true
                    },
                    body: JSON.stringify({
                        "/kpiInputType/view/1": {
                            "InputTypeOne": "bad input parameter - no operators"
                        }
                    })
                };
                console.log(response);
                resolve(response);
            }
        })
    } catch (err) {
        console.log(err);
    }

    function executeQuery(sql, connection, resolve, reject) {
        connection.query(sql, function (err, result) {
            console.log('sql' + sql);
            connection.release();
            if (err) reject(err);
            else {
                const response = {
                    statusCode: 200,
                    headers: {
                        "Access-Control-Allow-Origin": "*",
                        "Access-Control-Allow-Credentials": true
                    },
                    body: JSON.stringify({
                        "/kpiInputType/view/1": {
                            "InputTypeOne": result
                        }
                    })
                };
                console.log(response);
                resolve(response);
            }
        })
    }
}