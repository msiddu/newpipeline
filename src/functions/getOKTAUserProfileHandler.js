'use strict';
const pool = require('./config/config');
const sqlScripts = require('./config/sqlScripts');

module.exports.getOKTAUserProfileHandler = (event) => {
    try {
        return new Promise((resolve, reject) => {
            var OKTATysonID = '';
            //const data = JSON.parse(event.body);
            const data = event;
            console.log(data);
            OKTATysonID = data.OKTATysonID;
            pool.getConnection(function (err, connection) {
                console.log(OKTATysonID);
                if (err) reject(err);
                else {
                    var sql = '';
                    if (OKTATysonID) {
                        sql = sqlScripts.selectOKTAUserProfileSQL(OKTATysonID);
                        console.log(sql);
                        executeQuery(sql, connection, resolve, reject);
                    }else{
                        const response = {
                            statusCode: 400,
                            headers: {
                                "Access-Control-Allow-Origin": "*",
                                "Access-Control-Allow-Credentials": true
                            },
                            body: JSON.stringify({
                                "/security/integration/view": {
                                    "corpDetails": "bad input parameter - the Tyson Id is not valid"
                                }
                            })
                        };
                        console.log(response);
                        resolve(response);
                    }
                }
            })

        })
    } catch (err) {
        console.log(err);

    }

    function executeQuery(sql, connection, resolve, reject){
        connection.query(sql, function (err, result) {
            connection.release();
            if (err) reject(err);
            else {

                const response = {
                    statusCode: 200,
                    headers: {
                        "Access-Control-Allow-Origin": "*",
                        "Access-Control-Allow-Credentials": true
                    },
                    body: JSON.stringify({
                        "/security/integration/view": {
                            "corpDetails": result
                        }
                    })
                };
                console.log(response);
                resolve(response);
            }
        })
    }
}