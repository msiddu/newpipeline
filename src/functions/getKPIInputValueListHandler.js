'use strict';
const pool = require('./config/config');
const sqlScripts = require('./config/sqlScripts');


module.exports.getKPIInputValueListHandler = (event) => {

    try {
        return new Promise((resolve, reject) => {
            console.log(event);
            const data = JSON.parse(event.body);
            //const data = event;
            var KPIInputKey, KPIInputFiscalYear = '';
            KPIInputKey = data.KPIInputKey;
            KPIInputFiscalYear = data.KPIInputFiscalYear;
            pool.getConnection(function (err, connection) {
                if (err) reject(err);
                else {
                    if (!KPIInputFiscalYear || KPIInputFiscalYear == undefined
                        || KPIInputFiscalYear == null
                        || KPIInputKey == "" ||
                        !KPIInputKey || KPIInputKey == undefined
                        || KPIInputKey == null) {
                        const response = {
                            statusCode: 400,
                            headers: {
                                "Access-Control-Allow-Origin": "*",
                                "Access-Control-Allow-Credentials": true
                            },
                            body: JSON.stringify({
                                "/kpiInputValue/lview/All": {
                                    "InputValueAll": "bad input parameter - no operators"
                                }
                            })
                        };
                        console.log(response);
                        resolve(response);
                    }
                    else {
                        var sql = '';
                        sql = sqlScripts.selectKPIInputValueViewAllSQL(KPIInputKey, KPIInputFiscalYear);
                        console.log(sql);
                        executeQuery(sql, connection, resolve, reject);
                    }
                }
            })
        })
    } catch (err) {
        console.log(err);
    }

    function executeQuery(sql, connection, resolve, reject) {
        connection.query(sql, function (err, result) {
            console.log('sql' + sql);
            connection.release();
            if (err) reject(err);
            else {
                const response = {
                    statusCode: 200,
                    headers: {
                        "Access-Control-Allow-Origin": "*",
                        "Access-Control-Allow-Credentials": true
                    },
                    body: JSON.stringify({
                        "/kpiInputValue/lview/All": {
                            "InputValueAll": result
                        }
                    })
                };
                console.log(response);
                resolve(response);
            }
        })
    }
}