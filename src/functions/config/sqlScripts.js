module.exports = {
    //Sprint One Start
    selectOKTAUserProfileSQL: (oktaTysonID) => `select TYSON_ID as TysonId,USER_EMAIL as CorpEmail ,USER_NAME as CorpUserName ,USER_TITLE as  CorpUserTitle, 
         USER_ORG as CorpUserOrg ,USER_MGR as CorpUsrMgr ,USER_SKYPE as CorpUserSkype ,MOD_USER as ModUser ,date_format(MOD_DATE,'%Y-%m-%d')  as ModDate
         from okta_security  where TYSON_ID = '${oktaTysonID}' ;`,

    selectLocalUserProfileSQL: (oktaTysonID) => `select TYSON_ID as TysonId ,USER_NICKNAME as UserNickName,USER_PHONE as UserPhone, 
         USER_APP_CONTACT as UserContact,ENABLED_FLAG as UserEnabled ,USER_NOTES as UserNotes,
         MOD_USER as ModUser,MOD_DATE as ModDate 
         from security_local where TYSON_ID = '${oktaTysonID}';`,

    insertUserProfileSQL: (TysonId, UserNickName, UserPhone, UserContact, UserEnabled, UserNotes, ModUser) => `insert  into  security_local
          (TYSON_ID,USER_NICKNAME,USER_PHONE,USER_APP_CONTACT,ENABLED_FLAG,USER_NOTES,MOD_USER,MOD_DATE) 
          values('${TysonId}','${UserNickName}','${UserPhone}','${UserContact}','${UserEnabled}','${UserNotes}','${ModUser}',CURRENT_TIME());`,

    selectLocalUserProfileListSQL: () => `select TYSON_ID as TysonId,USER_EMAIL as CorpEmail,USER_NAME as CorpUserName,USER_TITLE  as CorpUserTitle,USER_ORG as CorpUserOrg ,
         MOD_USER as ModUser, MOD_DATE  as ModDate from security_list ;`,


         selectMessagesListSQL :(TysonId)  => `select MESSAGE_FROM_USER as MessageFromUserId,
         (select USER_NAME from okta_security where TYSON_ID = MESSAGE_FROM_USER) as MessageFromUserName,
         MESSAGE_TO_USER as MessageToUserId,
         (select USER_NAME from okta_security where TYSON_ID = MESSAGE_TO_USER) as MessageToUserName,
         date_format(MESSAGE_DATE,'%Y-%m-%d') as MessageDate,
         MESSAGE_TITLE as MessageTitle
         from MESSAGEHEADER  where '${TysonId}' in (MESSAGE_TO_USER,MESSAGE_FROM_USER) ;`,

         selectAllToMessagesListSQL :(TysonId)  => `select MESSAGE_FROM_USER as MessageFromUserId,
         (select USER_NAME from okta_security where TYSON_ID = MESSAGE_FROM_USER) as MessageFromUserName,
         MESSAGE_TO_USER as MessageToUserId,
         (select USER_NAME from okta_security where TYSON_ID = MESSAGE_TO_USER) as MessageToUserName,
         date_format(MESSAGE_DATE,'%Y-%m-%d') as MessageDate,
         MESSAGE_TITLE as MessageTitle
         from MESSAGEHEADER  where '${TysonId}' in (MESSAGE_TO_USER) ;`,

         selectAllFromMessagesListSQL :(TysonId)  => `select MESSAGE_FROM_USER as MessageFromUserId,
         (select USER_NAME from okta_security where TYSON_ID = MESSAGE_FROM_USER) as MessageFromUserName,
         MESSAGE_TO_USER as MessageToUserId,
         (select USER_NAME from okta_security where TYSON_ID = MESSAGE_TO_USER) as MessageToUserName,
         date_format(MESSAGE_DATE,'%Y-%m-%d') as MessageDate,
         MESSAGE_TITLE as MessageTitle
         from MESSAGEHEADER  where '${TysonId}' in (MESSAGE_FROM_USER) ;`,

         selectAllMessages : (TysonId) => 
         `select B.BROADCAST_DATE as MessageDate,
         B.BROADCAST_TITLE as MessageTitle,
         B.BROADCAST_DESC as MessageDesc,
         O.USER_NAME as MessageFromUserName,
         B.BROADCAST_SENT_FLAG as MassageSent
         from BROADCAST B inner join okta_security  O
         on O.TYSON_ID = B.BROADCAST_FROM_USER where BROADCAST_DATE <= CURRENT_TIME() order by MessageDate limit 1;
         select M.MESSAGE_FROM_USER as MessageFromUserId,
         (select USER_NAME from okta_security where TYSON_ID = M.MESSAGE_FROM_USER) as MessageFromUserName,
         M.MESSAGE_TO_USER as MessageToUserId,
         (select USER_NAME from okta_security where TYSON_ID = M.MESSAGE_TO_USER) as MessageToUserName,
         date_format(M.MESSAGE_DATE,'%Y-%m-%d') as MessageDate,
         M.MESSAGE_TITLE as MessageTitle,
		     M.MESSAGE_ACTION as MessageAction,
		    M.MESSAGE_DESC as MessageDesc,
		    M.MESSAGE_TYPE as MessageType,
		    M.MESSAGE_SENT_FLAG as MassageSent,
		    M.ENABLED_FLAG as MessageEnabled
         from MESSAGE M  where M.MESSAGE_TO_USER = '${TysonId}';
         `,

         selectSecureRolesList :()  => `select role_key as roleKey,
         user_role as userRole,
         user_role_desc as userRoleDesc
         from security_roles;`,

         editSecureUserRoles : (TysonID,UserRoleID,UserVLEKPI,ModUser) => `
         update security_user_roles
        set USER_ROLE_KEY = ${UserRoleID} ,USER_ROLE_VEKPI ='${UserVLEKPI}' ,
        MOD_USER ='${ModUser}' ,MOD_DATE = current_timestamp()
        where TYSON_ID = '${TysonID}'
         `,
        
        
        //Sprint 1 End.

    //Sprint 2 Start

    selectGroupViewListSQL: () => `select GROUP_KEY as GroupKey,GROUP_NAME as GroupName,GROUP_DESC as GroupDesc,GROUP_ORDER as GroupOrder
             from GROUP_VIEW order by GROUP_ORDER;`,

    selectGroupLogListSQL: () => ` SELECT  t.GROUP_KEY as GroupKey, t.GROUP_NAME as GroupName, t.GROUP_DESC as GroupDesc, t.GROUP_ORDER as GroupOrder,
    date_format(t.EFFECTIVE_DATE,'%Y%m%d')  as EffectiveDate,t.ENABLED as Enabled, (select USER_NAME from okta_security where TYSON_ID = t.MOD_USER) as ModUser,date_format(t.MOD_DATE,'%Y%m%d')  as ModDate
    FROM ( SELECT GROUP_KEY , MAX(MOD_DATE) AS max_mod_date FROM GROUP_LOG GROUP BY GROUP_KEY ) AS m
   INNER JOIN GROUP_LOG AS t ON t.GROUP_KEY = m.GROUP_KEY AND t.MOD_DATE = m.max_mod_date;`,

    selectGroupLogSQL: (GroupKey) => `SELECT  a.GROUP_KEY as GroupKey, a.GROUP_NAME as GroupName, a.GROUP_DESC as GroupDesc, a.GROUP_ORDER as GroupOrder,
    date_format(a.EFFECTIVE_DATE,'%Y-%m-%d')  as EffectiveDate,a.ENABLED as Enabled, (select USER_NAME from okta_security where TYSON_ID = a.MOD_USER) as ModUser,date_format(a.MOD_DATE,'%Y-%m-%d')  as ModDate
    FROM GROUP_LOG a INNER JOIN (SELECT GROUP_KEY, MAX(MOD_DATE) MOD_DATE FROM GROUP_LOG GROUP BY GROUP_KEY
    ) b ON a.GROUP_KEY = b.GROUP_KEY AND a.MOD_DATE = b.MOD_DATE and a.GROUP_KEY = ${GroupKey}`,

    insertGroupLogSQL: (GroupName, GroupDesc, GroupOrder, ModUser, EffectiveDate, Enabled) =>
        `BEGIN;
    INSERT INTO GROUP_VIEW (GROUP_NAME,GROUP_DESC,GROUP_ORDER)
      VALUES('${GroupName}','${GroupDesc}',${GroupOrder});
    INSERT INTO GROUP_LOG (GROUP_KEY,GROUP_NAME,GROUP_DESC,GROUP_ORDER,MOD_USER,MOD_DATE,EFFECTIVE_DATE,ENABLED) 
      VALUES(last_insert_id(),'${GroupName}','${GroupDesc}',${GroupOrder},'${ModUser}',CURRENT_TIME(),'${EffectiveDate}','${Enabled}');
      Select USER_NAME from okta_security where TYSON_ID = '${ModUser}';
    COMMIT;`,

    editGroupLogSQL: (GroupKey, GroupName, GroupDesc, GroupOrder, ModUser, EffectiveDate, Enabled) =>
        `BEGIN ;
    update GROUP_VIEW set GROUP_NAME = '${GroupName}' , GROUP_DESC = '${GroupDesc}' ,
    GROUP_ORDER = ${GroupOrder} where GROUP_KEY = ${GroupKey};
    INSERT INTO GROUP_LOG (GROUP_KEY,GROUP_NAME,GROUP_DESC,GROUP_ORDER,MOD_USER,MOD_DATE,EFFECTIVE_DATE,ENABLED) 
    VALUES(${GroupKey},'${GroupName}','${GroupDesc}',${GroupOrder},'${ModUser}',CURRENT_TIME(),'${EffectiveDate}','${Enabled}');
    Select USER_NAME from okta_security where TYSON_ID = '${ModUser}';
   COMMIT;`,

    selectFamilyViewList: (GroupKey) =>
        `select  GROUP_KEY as GroupKey,FAMILY_KEY as FamilyKey,FAMILY_NAME as FamilyName,
      FAMILY_DESC as FamilyDesc,FAMILY_ORDER as FamilyOrder from FAMILY_VIEW where GROUP_KEY = ${GroupKey};`,
    
    selectFamilyLogSQL : (GroupKey,FamilyKey) => `select  GROUP_KEY as GroupKey,FAMILY_KEY as FamilyKey,
    FAMILY_NAME as FamilyName,FAMILY_DESC as FamilyDesc,FAMILY_ORDER as FamilyOrder,
    date_format(EFFECTIVE_DATE,'%Y-%m-%d') as EffectiveDate,
    ENABLED as Enabled,date_format(MOD_DATE,'%Y-%m-%d') as ModDate,
    (select USER_NAME from okta_security where TYSON_ID = F.MOD_USER ) as ModUser
    from  FAMILY_LOG F where GROUP_KEY = ${GroupKey}  and FAMILY_KEY= ${FamilyKey} 
    order by MOD_DATE DESC Limit 1 ;`,

    selectFamilyLogList :(GroupKey)  => 
    `SELECT  a.GROUP_KEY as GroupKey, a.FAMILY_KEY as FamilyKey, a.FAMILY_NAME as FamilyName,
    a.FAMILY_DESC as FamilyDesc,a.FAMILY_ORDER as FamilyOrder,
    date_format(a.MOD_DATE,'%Y%m%d')  as ModDate,
    (select USER_NAME from okta_security where TYSON_ID = a.MOD_USER) as ModUser,
    date_format(a.MOD_DATE,'%Y-%m-%d')  as ModDate,a.ENABLED as Enabled 
    FROM FAMILY_LOG a INNER JOIN (SELECT GROUP_KEY,FAMILY_KEY, MAX(MOD_DATE) MOD_DATE FROM FAMILY_LOG 
    where GROUP_KEY = ${GroupKey} GROUP BY FAMILY_KEY
    ) b ON a.GROUP_KEY = b.GROUP_KEY AND a.MOD_DATE = b.MOD_DATE
    and a.FAMILY_KEY = b.FAMILY_KEY and a.GROUP_KEY = ${GroupKey};`,

    insertFamilyLogSQL : (GroupKey,FamilyName,FamilyDesc,FamilyOrder,ModUser,EffectiveDate,Enabled) =>
    ` BEGIN;
    select IFNULL(max(FAMILY_KEY)+1,1) from FAMILY_VIEW 
     INTO @latest_family_key;
    INSERT INTO FAMILY_VIEW (GROUP_KEY,FAMILY_KEY,FAMILY_NAME,
    FAMILY_DESC,FAMILY_ORDER)
    VALUES(${GroupKey},@latest_family_key,'${FamilyName}',
    '${FamilyDesc}',${FamilyOrder});
    INSERT INTO FAMILY_LOG (GROUP_KEY,FAMILY_KEY,FAMILY_NAME,
    FAMILY_DESC,FAMILY_ORDER,MOD_USER,MOD_DATE,EFFECTIVE_DATE,ENABLED) 
    VALUES(${GroupKey},@latest_family_key,'${FamilyName}',
    '${FamilyDesc}',${FamilyOrder},'${ModUser}',
    CURRENT_TIME(),'${EffectiveDate}','${Enabled}');
    Select USER_NAME from okta_security where TYSON_ID = '${ModUser}';
    COMMIT ;`,

    editFamilyLogSQL : (GroupKey,FamilyKey,FamilyName,FamilyDesc,FamilyOrder,ModUser,EffectiveDate,Enabled) =>
    ` BEGIN;
    UPDATE  FAMILY_VIEW  set GROUP_KEY = ${GroupKey} ,
    FAMILY_KEY = ${FamilyKey},FAMILY_NAME = '${FamilyName}',
    FAMILY_DESC = '${FamilyDesc}',FAMILY_ORDER = ${FamilyOrder}
    WHERE GROUP_KEY = ${GroupKey} and  FAMILY_KEY = ${FamilyKey} ;
    INSERT INTO FAMILY_LOG (GROUP_KEY,FAMILY_KEY,FAMILY_NAME,
    FAMILY_DESC,FAMILY_ORDER,MOD_USER,MOD_DATE,EFFECTIVE_DATE,ENABLED) 
    VALUES(${GroupKey},${FamilyKey},'${FamilyName}',
    '${FamilyDesc}',${FamilyOrder},'${ModUser}',
    CURRENT_TIME(),'${EffectiveDate}','${Enabled}');
    Select USER_NAME from okta_security where TYSON_ID = '${ModUser}';
    COMMIT ;`,

    selectTypeViewListSQL :()  => 
    `select TYPE_KEY as TypeKey,TYPE_NAME as TypeName,TYPE_DESC as TypeDesc,TYPE_ORDER as TypeOrder from TYPE_VIEW;`,
 
    selectTypeLogSQL :(TypeKey) => `select TYPE_KEY as TypeKey,TYPE_NAME as TypeName,TYPE_DESC as TypeDesc,
    TYPE_ORDER as TypeOrder,date_format(EFFECTIVE_DATE,'%Y-%m-%d') as EffectiveDate,ENABLED as Enabled,date_format(MOD_DATE,'%Y-%m-%d') as ModDate,MOD_USER as ModUser
     from TYPE_LOG where TYPE_KEY = ${TypeKey} order by MOD_DATE desc limit  1;
    `,
 
    selectTypeLogListSQL : () => `SELECT t.TYPE_KEY as TypeKey,t.TYPE_NAME as  TypeName,t.TYPE_DESC as TypeDesc,t.TYPE_ORDER as TypeOrder,
    date_format(t.EFFECTIVE_DATE,'%Y%m%d') as EffectiveDate,t.ENABLED as Enabled,t.MOD_USER as ModUser,date_format(t.MOD_DATE,'%Y%m%d') as ModDate
   FROM ( SELECT TYPE_KEY , MAX(MOD_DATE) AS max_mod_date FROM TYPE_LOG GROUP BY TYPE_KEY ) AS m
   INNER JOIN TYPE_LOG AS t ON t.TYPE_KEY = m.TYPE_KEY AND t.MOD_DATE = m.max_mod_date;`,

   insertTypeLogSQL : (TypeName,TypeDesc,TypeOrder,ModUser,EffectiveDate,Enabled) => `
   BEGIN;
   INSERT INTO TYPE_VIEW (TYPE_NAME,TYPE_DESC,TYPE_ORDER)
   VALUES('${TypeName}','${TypeDesc}',${TypeOrder});
   INSERT INTO TYPE_LOG (TYPE_KEY,TYPE_NAME,TYPE_DESC,TYPE_ORDER,MOD_USER,MOD_DATE,EFFECTIVE_DATE,ENABLED) 
   VALUES(last_insert_id(),'${TypeName}','${TypeDesc}',${TypeOrder},'${ModUser}',CURRENT_TIME(),'${EffectiveDate}','${Enabled}');
   Select USER_NAME from okta_security where TYSON_ID = '${ModUser}';
   COMMIT;`,

   editTypeLogSQL : (TypeKey,TypeName,TypeDesc,TypeOrder,ModUser,EffectiveDate,Enabled) =>`
   BEGIN;
   UPDATE  TYPE_VIEW set TYPE_KEY = ${TypeKey},TYPE_NAME ='${TypeName}' ,
   TYPE_DESC = '${TypeDesc}',TYPE_ORDER = ${TypeOrder} where TYPE_KEY = ${TypeKey};
   INSERT INTO TYPE_LOG (TYPE_KEY,TYPE_NAME,TYPE_DESC,TYPE_ORDER,MOD_USER,MOD_DATE,EFFECTIVE_DATE,ENABLED) 
   VALUES(${TypeKey},'${TypeName}','${TypeDesc}',${TypeOrder},'${ModUser}',CURRENT_TIME()
   ,'${EffectiveDate}','${Enabled}');
   Select USER_NAME from okta_security where TYSON_ID = '${ModUser}';
   COMMIT;
   `,

    //Sprint 2 End

  //Sprint 3 start


     selectKPIInputTypeSQL : (KPI_Fiscal_Year,KPIInputTypeKey) => `select 
      KPI_INPUT_TYPE_KEY as KPIInputTypeKey,
      KPI_INPUT_FISCAL_YEAR as KPIInputFiscalYear,
      KPI_INPUT_TYPE_NAME as KPIInputTypeName,
      KPI_INPUT_TYPE_DESC as KPIInputTypeDesc,
      KPI_INPUT_TYPE_ORDER as KPIInputOrder
      from KPI_INPUT_TYPE
      where KPI_INPUT_FISCAL_YEAR ='${KPI_Fiscal_Year}'  and KPI_INPUT_TYPE_KEY = ${KPIInputTypeKey};`,
      selectKPIInputTypeListSQL : (KPI_Fiscal_Year) => `select 
      KPI_INPUT_TYPE_KEY as KPIInputTypeKey,
      KPI_INPUT_FISCAL_YEAR as KPIInputFiscalYear,
      KPI_INPUT_TYPE_NAME as KPIInputTypeName,
      KPI_INPUT_TYPE_DESC as KPIInputTypeDesc,
      KPI_INPUT_TYPE_ORDER as KPIInputOrder
      from KPI_INPUT_TYPE
      where KPI_INPUT_FISCAL_YEAR ='${KPI_Fiscal_Year}'  order by KPI_INPUT_TYPE_KEY ;`,
     

       selectKPIInputValue : (KPIInputKey,KPIInputTypeKey,CurrentInd) => `select KPI.MOD_INPUT_USER AS TysonId,
       KPI.KPI_INPUT_KEY as KPIInputKey,
      KPI.KPI_INPUT_TYPE_KEY as KPIInputTypeKey,
      KPI.KPI_INPUT_VALUE as KPIInputValue,
      KPI.CURRENT_IND as CurrentInd,
      o.USER_NAME as ModUser,
      KPI.MOD_INPUT_DATE as ModDate
       from KPI_INPUT_VALUE KPI
       left outer join okta_security o
       on o.TYSON_ID = KPI.MOD_INPUT_USER
       where  KPI.KPI_INPUT_KEY = ${KPIInputKey} and 
      KPI.KPI_INPUT_TYPE_KEY  =  ${KPIInputTypeKey} and
      KPI.CURRENT_IND = '${CurrentInd}';
       `,

       selectKPIInputApproved : (KPIInputKey,KPIInputTypeKey,CurrentInd) => `select KPI.MOD_INPUT_USER AS TysonId,
       KPI.KPI_INPUT_KEY as KPIInputKey,
      KPI.KPI_INPUT_TYPE_KEY as KPIInputTypeKey,
      KPI.KPI_INPUT_APPROVED as KPIInputApproved,
      KPI.CURRENT_IND as CurrentInd,
      o.USER_NAME as ModUser,
      KPI.MOD_INPUT_DATE as ModDate
       from KPI_INPUT_APPROVED KPI
       left outer join okta_security o
       on o.TYSON_ID = KPI.MOD_INPUT_USER
       where  KPI.KPI_INPUT_KEY = ${KPIInputKey} and 
       KPI.KPI_INPUT_TYPE_KEY  =  ${KPIInputTypeKey} and
       KPI.CURRENT_IND = '${CurrentInd}';
        `,

        selectKPIInputLocked : (KPIInputKey,KPIInputTypeKey,CurrentInd) =>`
        select KPI.MOD_INPUT_USER AS TysonId,
   KPI.KPI_INPUT_KEY as KPIInputKey,
  KPI.KPI_INPUT_TYPE_KEY as KPIInputTypeKey,
  KPI.KPI_INPUT_LOCKED as KPIInputLocked,
  KPI.CURRENT_IND as CurrentInd,
  o.USER_NAME as ModUser,
  KPI.MOD_INPUT_DATE as ModDate
   from KPI_INPUT_LOCKED KPI
   left outer join okta_security o
   on o.TYSON_ID = KPI.MOD_INPUT_USER
   where  KPI.KPI_INPUT_KEY = ${KPIInputKey} and 
   KPI.KPI_INPUT_TYPE_KEY  =  ${KPIInputTypeKey} and
   KPI.CURRENT_IND = '${CurrentInd}';
        `,

        insertKPIInputValueSQL : (KPIInputKey,KPIInputTypeKey,KPIInputValue,ModInputDate,TysonId,CurrentInd) => `
        BEGIN;
       Update KPI_INPUT_VALUE set CURRENT_IND='N'  where KPI_INPUT_KEY =${KPIInputKey}  and KPI_INPUT_TYPE_KEY=${KPIInputTypeKey};
       INSERT into KPI_INPUT_VALUE 
       (KPI_INPUT_KEY,KPI_INPUT_TYPE_KEY,KPI_INPUT_VALUE,MOD_INPUT_DATE,MOD_INPUT_USER,CURRENT_IND)  
        values (${KPIInputKey},${KPIInputTypeKey},'${KPIInputValue}',current_timestamp(),'${TysonId}','${CurrentInd}');
       COMMIT;`,
     
        insertKPIInputApproveSQL : (KPIInputKey,KPIInputTypeKey,KPIInputApproved,ModInputDate,TysonId,CurrentInd) => `
        BEGIN;
       Update KPI_INPUT_APPROVED set CURRENT_IND='N'  where KPI_INPUT_KEY =${KPIInputKey}  and KPI_INPUT_TYPE_KEY=${KPIInputTypeKey};
       INSERT into KPI_INPUT_APPROVED 
       (KPI_INPUT_KEY,KPI_INPUT_TYPE_KEY,KPI_INPUT_APPROVED,MOD_INPUT_DATE,MOD_INPUT_USER,CURRENT_IND)  
        values (${KPIInputKey},${KPIInputTypeKey},'${KPIInputApproved}',current_timestamp(),'${TysonId}','${CurrentInd}');
       COMMIT;`,
     
     insertKPIInputLockSQL : (KPIInputKey,KPIInputTypeKey,KPIInputLocked,ModInputDate,TysonId,CurrentInd) => `
     BEGIN;
     Update KPI_INPUT_LOCKED set CURRENT_IND='N'  where KPI_INPUT_KEY =${KPIInputKey}  and KPI_INPUT_TYPE_KEY=${KPIInputTypeKey};
     INSERT into KPI_INPUT_LOCKED 
     (KPI_INPUT_KEY,KPI_INPUT_TYPE_KEY,KPI_INPUT_LOCKED,MOD_INPUT_DATE,MOD_INPUT_USER,CURRENT_IND)  
     values (${KPIInputKey},${KPIInputTypeKey},'${KPIInputLocked}',current_timestamp(),'${TysonId}','${CurrentInd}');
     COMMIT;
       `,
     selectKPIInputValueViewAllSQL : (KPIInputKey,KPIInputFiscalYear) => 
     `select KPA.KPI_INPUT_KEY as KPIInputKey,
     KPV.KPI_INPUT_TYPE_KEY as KPIInputTypeKey,
     KPT.KPI_INPUT_TYPE_NAME as KPIInputTypeName,
     KPV.KPI_INPUT_VALUE as KPIInputValue,
     KPA.KPI_INPUT_APPROVED as KPIInputApproved,
     KPL.KPI_INPUT_LOCKED as KPIInputLocked
      from KPI_INPUT_VALUE KPV
     left outer join KPI_INPUT_APPROVED KPA
     on KPV.KPI_INPUT_KEY = KPA.KPI_INPUT_KEY and
     KPV.KPI_INPUT_TYPE_KEY = KPA.KPI_INPUT_TYPE_KEY and KPV.CURRENT_IND = 'Y' and KPA.CURRENT_IND = 'Y'  
     left outer join KPI_INPUT_LOCKED KPL
     on KPL.KPI_INPUT_KEY = KPA.KPI_INPUT_KEY and
     KPL.KPI_INPUT_TYPE_KEY = KPA.KPI_INPUT_TYPE_KEY and KPL.CURRENT_IND = 'Y'
     left outer join KPI_INPUT_TYPE KPT
     on KPT.KPI_INPUT_TYPE_KEY = KPA.KPI_INPUT_TYPE_KEY
     where KPA.KPI_INPUT_KEY = ${KPIInputKey} and KPT.KPI_INPUT_FISCAL_YEAR = '${KPIInputFiscalYear}'`,

selectKPIFamilyInputValueViewSQL : (KPIInputKey,KPIInputFiscalYear,FamilyKey) => `
select KV.FAMILY_KEY as FamyKey,
KPA.KPI_INPUT_KEY as KPIInputKey,
KPV.KPI_INPUT_TYPE_KEY as KPIInputTypeKey,
KPT.KPI_INPUT_TYPE_NAME as KPIInputTypeName,
KPV.KPI_INPUT_VALUE as KPIInputValue,
KPA.KPI_INPUT_APPROVED as KPIInputApproved,
KPL.KPI_INPUT_LOCKED as KPIInputLocked
from KPI_INPUT_VALUE KPV
left outer join KPI_INPUT_APPROVED KPA
on KPV.KPI_INPUT_KEY = KPA.KPI_INPUT_KEY and
KPV.KPI_INPUT_TYPE_KEY = KPA.KPI_INPUT_TYPE_KEY
and KPV.CURRENT_IND = 'Y' 
and KPA.CURRENT_IND = 'Y'  
left outer join KPI_INPUT_LOCKED KPL
on KPL.KPI_INPUT_KEY = KPA.KPI_INPUT_KEY and
KPL.KPI_INPUT_TYPE_KEY = KPA.KPI_INPUT_TYPE_KEY and KPL.CURRENT_IND = 'Y'
left outer join KPI_INPUT_TYPE KPT
on KPT.KPI_INPUT_TYPE_KEY = KPA.KPI_INPUT_TYPE_KEY
left outer join KPI_VIEW KV 
on KV.KPI_KEY = KPV.KPI_INPUT_KEY 
where KPA.KPI_INPUT_KEY = ${KPIInputKey} 
and KPT.KPI_INPUT_FISCAL_YEAR = '${KPIInputFiscalYear}'
and KV.FAMILY_KEY = ${FamilyKey};
`,

selectKPIGroupInputValueViewSQL : (KPIInputKey,KPIInputFiscalYear,GroupKey) => `
select KV.FAMILY_KEY as FamyKey,
KPA.KPI_INPUT_KEY as KPIInputKey,
KPV.KPI_INPUT_TYPE_KEY as KPIInputTypeKey,
KPT.KPI_INPUT_TYPE_NAME as KPIInputTypeName,
KPV.KPI_INPUT_VALUE as KPIInputValue,
KPA.KPI_INPUT_APPROVED as KPIInputApproved,
KPL.KPI_INPUT_LOCKED as KPIInputLocked
from KPI_INPUT_VALUE KPV
left outer join KPI_INPUT_APPROVED KPA
on KPV.KPI_INPUT_KEY = KPA.KPI_INPUT_KEY and
KPV.KPI_INPUT_TYPE_KEY = KPA.KPI_INPUT_TYPE_KEY 
and KPV.CURRENT_IND = 'Y' and KPA.CURRENT_IND = 'Y'  
left outer join KPI_INPUT_LOCKED KPL
on KPL.KPI_INPUT_KEY = KPA.KPI_INPUT_KEY and
KPL.KPI_INPUT_TYPE_KEY = KPA.KPI_INPUT_TYPE_KEY 
and KPL.CURRENT_IND = 'Y'
left outer join KPI_INPUT_TYPE KPT
on KPT.KPI_INPUT_TYPE_KEY = KPA.KPI_INPUT_TYPE_KEY
left outer join KPI_VIEW KV 
on KV.KPI_KEY = KPV.KPI_INPUT_KEY 
where KPA.KPI_INPUT_KEY = ${KPIInputKey} 
and KPT.KPI_INPUT_FISCAL_YEAR = '${KPIInputFiscalYear}'
and KV.GROUP_KEY = ${GroupKey};
`,

  //Sprint 3 End
}
