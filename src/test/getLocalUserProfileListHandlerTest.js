var supertest = require('supertest');
// eslint-disable-next-line no-unused-vars
var should = require('should');
var server = supertest.agent("https://xzrt3bk0zh.execute-api.us-east-2.amazonaws.com/tpma-dev-gateway");

// eslint-disable-next-line no-undef
describe("TPMA Local User Profile List View Services Unit Test", function () {
// eslint-disable-next-line no-undef
    it("To Test User Profile  Service which fetches User Profiles List from Local Data Base",
    function (done) {
        server
            .get("/security/corplocal/list")
            .expect('Content-type', 'application/json')
            .expect(200)
            .end(function (err, res) {
                res.status.should.equal(200);
                console.log(JSON.stringify(res.body));
                done();
            })
    })

       
});

